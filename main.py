# -*- coding: utf-8 -*-
import datetime
import random
import sys
import threading
import time

from PyQt5.Qt import *
from sudokugen.generator import generate, Difficulty, MaxRetriesExceeded
from functools import partial


class QHLine(QFrame):
    def __init__(self):
        super(QHLine, self).__init__()
        self.setFrameShape(QFrame.HLine)
        self.setFrameShadow(QFrame.Sunken)


class QVLine(QFrame):
    def __init__(self):
        super(QVLine, self).__init__()
        self.setFrameShape(QFrame.VLine)
        self.setFrameShadow(QFrame.Sunken)


class Main(QMainWindow):
    def __init__(self):
        super(Main, self).__init__()
        self.difficulty = Difficulty.EASY
        self.wait_for_keypress = False
        self.current_button = None
        self.old_button_text = ""
        self.current_button_row = 0
        self.current_button_col = 0
        self.fails = 0
        self.max_fails = 10
        self.time_counter = 0
        self.solution = []
        self.buttons = []
        self.fail_enabled = True
        self.show_solution_enabled = True
        self.timer_stop = False
        self.timer = threading.Thread(target=self.timer_thread)
        self.timer.start()
        self.load_ui()
        self.generate_fields()

    def timer_thread(self):
        self.timer = datetime.timedelta(seconds=0, minutes=0)
        self.time_counter = 0
        while True:
            self.time_counter += 1
            time.sleep(1)
            self.timer_display.setText("Zeit: " + str(self.timer + datetime.timedelta(seconds=self.time_counter)))
            if not threading.main_thread().is_alive():
                return False

    def load_ui(self):
        exit_action = QAction("Beenden", self)
        exit_action.setShortcut("Alt+F4")
        exit_action.triggered.connect(app.quit)
        new_action = QAction("Neues Spiel", self)
        new_action.triggered.connect(self.dialog_new)
        new_action.setShortcut("Ctrl+N")
        self.easy_action = QAction("Einfach", self)
        self.easy_action.triggered.connect(self.set_easy)
        self.easy_action.setCheckable(True)
        self.easy_action.setChecked(True)
        self.medium_action = QAction("Mittel", self)
        self.medium_action.setCheckable(True)
        self.medium_action.triggered.connect(self.set_medium)
        self.hard_action = QAction("Schwer", self)
        self.hard_action.setCheckable(True)
        self.hard_action.triggered.connect(self.set_hard)
        menubar = self.menuBar()
        game_menu = menubar.addMenu('Spiel')
        game_menu.addAction(new_action)
        self.difficulty_display = QAction("Schwierigkeit: Einfach", self)
        menubar.addAction(self.difficulty_display)
        self.timer_display = QAction("Zeit: 0:00:00", self)
        menubar.addAction(self.timer_display)
        game_menu.addSeparator()
        difficulty_menu = game_menu.addMenu("Schwierigkeit")
        difficulty_menu.addAction(self.easy_action)
        difficulty_menu.addAction(self.medium_action)
        difficulty_menu.addAction(self.hard_action)
        self.enable_show_solution = QAction("Richtigkeit anzeigen")
        self.enable_show_solution.setCheckable(True)
        self.enable_show_solution.setChecked(True)
        self.enable_show_solution.triggered.connect(self.set_show_solution)
        fail_menu = game_menu.addMenu("Verlieren")
        self.enable_fail = QAction("Aktivieren")
        self.enable_fail.setCheckable(True)
        self.enable_fail.triggered.connect(self.set_fail)
        self.enable_fail.setChecked(True)
        fail_menu.addAction(self.enable_fail)
        self.fail_3 = QAction("Max. 3 Fehler")
        self.fail_3.setCheckable(True)
        self.fail_3.triggered.connect(self.set_fail_3)
        self.fail_5 = QAction("Max. 5 Fehler")
        self.fail_5.setCheckable(True)
        self.fail_5.triggered.connect(self.set_fail_5)
        self.fail_10 = QAction("Max. 10 Fehler")
        self.fail_10.setCheckable(True)
        self.fail_10.setChecked(True)
        self.fail_10.triggered.connect(self.set_fail_10)
        fail_menu.addAction(self.fail_3)
        fail_menu.addAction(self.fail_5)
        fail_menu.addAction(self.fail_10)
        game_menu.addAction(self.enable_show_solution)
        game_menu.addSeparator()
        game_menu.addAction(exit_action)
        self.setFixedSize(800, 800)
        self.setWindowTitle("Sudoku")
        main_widget = QWidget()
        self.setCentralWidget(main_widget)
        self.grid_layout = QGridLayout()
        main_widget.setLayout(self.grid_layout)
        self.show()

    def dialog_settings_change(self):
        dialog = QMessageBox()
        dialog.setIcon(QMessageBox.Warning)
        dialog.setText("Die geänderten Einstellungen werden erst bei einem neuen Spiel verwendet. Möchten Sie ein neues Spiel starten?")
        dialog.setWindowTitle("Einstellung ändern")
        dialog.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        if dialog.exec() == QMessageBox.Yes:
            return True
        else:
            return False

    def set_fail_3(self):
        self.fail_3.setChecked(True)
        self.fail_5.setChecked(False)
        self.fail_10.setChecked(False)
        self.max_fails = 3
        if self.dialog_settings_change():
            self.generate_fields()

    def set_fail_5(self):
        self.fail_3.setChecked(False)
        self.fail_5.setChecked(True)
        self.fail_10.setChecked(False)
        self.max_fails = 5
        if self.dialog_settings_change():
            self.generate_fields()

    def set_fail_10(self):
        self.fail_3.setChecked(False)
        self.fail_5.setChecked(False)
        self.fail_10.setChecked(True)
        self.max_fails = 10
        if self.dialog_settings_change():
            self.generate_fields()

    def set_fail(self):
        self.fail_enabled = self.enable_fail.isChecked()
        if self.dialog_settings_change():
            self.generate_fields()

    def set_show_solution(self):
        self.show_solution_enabled = self.enable_show_solution.isChecked()
        if self.dialog_settings_change():
            self.generate_fields()

    def dialog_new(self):
        dialog = QMessageBox()
        dialog.setIcon(QMessageBox.Warning)
        dialog.setText("Um ein neues Spiel zu starten, wird aktuelle Spiel wird unterbrochen. Möchten Sie fortfahren?")
        dialog.setWindowTitle("Neues Spiel")
        dialog.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        if dialog.exec() == QMessageBox.Yes:
            self.generate_fields()

    def set_easy(self):
        self.difficulty = Difficulty.EASY
        self.easy_action.setChecked(True)
        self.medium_action.setChecked(False)
        self.hard_action.setChecked(False)
        self.difficulty_display.setText("Schwierigkeit: Einfach")
        if self.dialog_settings_change():
            self.generate_fields()

    def set_medium(self):
        self.difficulty = Difficulty.MEDIUM
        self.easy_action.setChecked(False)
        self.medium_action.setChecked(True)
        self.hard_action.setChecked(False)
        self.difficulty_display.setText("Schwierigkeit: Mittel")
        if self.dialog_settings_change():
            self.generate_fields()

    def set_hard(self):
        self.difficulty = Difficulty.HARD
        self.easy_action.setChecked(False)
        self.medium_action.setChecked(False)
        self.hard_action.setChecked(True)
        self.difficulty_display.setText("Schwierigkeit: Schwer")
        if self.dialog_settings_change():
            self.generate_fields()

    def keyPressEvent(self, event):
        if self.wait_for_keypress:
            if event.key() == Qt.Key_1:
                self.wait_for_keypress = False
                self.check_button(1)
            elif event.key() == Qt.Key_2:
                self.wait_for_keypress = False
                self.check_button(2)
            elif event.key() == Qt.Key_3:
                self.wait_for_keypress = False
                self.check_button(3)
            elif event.key() == Qt.Key_4:
                self.wait_for_keypress = False
                self.check_button(4)
            elif event.key() == Qt.Key_5:
                self.wait_for_keypress = False
                self.check_button(5)
            elif event.key() == Qt.Key_6:
                self.wait_for_keypress = False
                self.check_button(6)
            elif event.key() == Qt.Key_7:
                self.wait_for_keypress = False
                self.check_button(7)
            elif event.key() == Qt.Key_8:
                self.wait_for_keypress = False
                self.check_button(8)
            elif event.key() == Qt.Key_9:
                self.wait_for_keypress = False
                self.check_button(9)
            else:
                self.current_button.setText(self.old_button_text)
                self.current_button.setStyleSheet("font-size: 20px;")
                self.wait_for_keypress = False

    def button_press(self, button, row, col):
        self.wait_for_keypress = True
        self.current_button = button
        self.current_button_row = row
        self.current_button_col = col
        self.old_button_text = button.text()
        button.setText("...")
        button.setStyleSheet("color: gray; font-size: 20px;")

    def check_button(self, number):
        self.current_button.setText(str(number))
        if number == self.solution[self.current_button_row][self.current_button_col]:
            if self.show_solution_enabled:
                self.current_button.setStyleSheet("color: green; font-size: 20px;")
            else:
                self.current_button.setStyleSheet("font-size: 20px;")
            self.current_button.setWhatsThis("good")
        else:
            if self.show_solution_enabled:
                self.current_button.setStyleSheet("color: red; font-size: 20px;")
            else:
                self.current_button.setStyleSheet("font-size: 20px;")
            self.fails += 1
            if self.fails == self.max_fails and self.fail_enabled:
                dialog = QMessageBox()
                dialog.setIcon(QMessageBox.Information)
                dialog.setText("Sie haben das Spiel verloren! Möchten Sie ein neues Spiel starten?")
                dialog.setWindowTitle("Spiel verloren!")
                dialog.setStandardButtons(QMessageBox.Yes | QMessageBox.Close)
                if dialog.exec() == QMessageBox.Yes:
                    self.generate_fields()
                else:
                    app.quit()
            self.current_button.setWhatsThis("bad")

        finished = True
        for button in self.buttons:
            if button.text() == "" or button.text() == "..." or button.whatsThis() == "bad":
                finished = False

        if finished:
            self.finish_game()

    def finish_game(self):
        dialog = QMessageBox()
        dialog.setIcon(QMessageBox.Information)
        dialog.setText("Sie haben das Spiel gewonnen! Möchten Sie ein neues Spiel starten?")
        dialog.setWindowTitle("Spiel gewonnen!")
        dialog.setStandardButtons(QMessageBox.Yes | QMessageBox.Close)
        if dialog.exec() == QMessageBox.Yes:
            self.generate_fields()
        else:
            app.quit()

    def generate_fields(self):
        self.timer = datetime.timedelta(seconds=0, minutes=0)
        self.time_counter = 0
        self.fails = 0
        try:
            sudoku, solution = generate(difficulty=self.difficulty)
            self.solution = solution
            self.buttons = []
            for i in reversed(range(self.grid_layout.count())):
                self.grid_layout.itemAt(i).widget().setParent(None)

            if 0 not in sudoku:
                sudoku_temp = []
                for number in sudoku:
                    if random.randint(1, 10) == 5:
                        sudoku_temp.append(0)
                    else:
                        sudoku_temp.append(number)
                    sudoku = sudoku_temp

            for number in range(len(sudoku)):
                number_string = str(sudoku[number])
                row = number // 9
                col = number % 9
                if number_string == "0":
                    number_string = ""
                button = QPushButton(number_string)
                if number_string == "":
                    button.clicked.connect(partial(self.button_press, button, row, col))
                button.setFixedSize(60, 60)
                button.setStyleSheet("font-size: 20px;")
                self.buttons.append(button)

                if row <= 1:
                    if col <= 1:
                        self.grid_layout.addWidget(button, row, col)
                    elif col == 2:
                        self.grid_layout.addWidget(button, row, col)
                        self.grid_layout.addWidget(QVLine(), row, col + 1)
                    elif 3 <= col <= 4:
                        self.grid_layout.addWidget(button, row, col + 1)
                    elif col == 5:
                        self.grid_layout.addWidget(button, row, col + 1)
                        self.grid_layout.addWidget(QVLine(), row, col + 2)
                    elif 6 <= col <= 8:
                        self.grid_layout.addWidget(button, row, col + 2)

                elif row == 2:
                    if col <= 1:
                        self.grid_layout.addWidget(button, row, col)
                        self.grid_layout.addWidget(QHLine(), row + 1, col)
                    elif col == 2:
                        self.grid_layout.addWidget(button, row, col)
                        self.grid_layout.addWidget(QVLine(), row, col + 1)
                        self.grid_layout.addWidget(QHLine(), row + 1, col)
                    elif 3 <= col <= 4:
                        self.grid_layout.addWidget(button, row, col + 1)
                        self.grid_layout.addWidget(QHLine(), row + 1, col + 1)
                    elif col == 5:
                        self.grid_layout.addWidget(button, row, col + 1)
                        self.grid_layout.addWidget(QVLine(), row, col + 2)
                        self.grid_layout.addWidget(QHLine(), row + 1, col + 1)
                    elif 6 <= col <= 8:
                        self.grid_layout.addWidget(button, row, col + 2)
                        self.grid_layout.addWidget(QHLine(), row + 1, col + 2)

                elif 3 <= row <= 4:
                    if col <= 1:
                        self.grid_layout.addWidget(button, row + 1, col)
                    elif col == 2:
                        self.grid_layout.addWidget(button, row + 1, col)
                        self.grid_layout.addWidget(QVLine(), row + 1, col + 1)
                    elif 3 <= col <= 4:
                        self.grid_layout.addWidget(button, row + 1, col + 1)
                    elif col == 5:
                        self.grid_layout.addWidget(button, row + 1, col + 1)
                        self.grid_layout.addWidget(QVLine(), row + 1, col + 2)
                    elif 6 <= col <= 8:
                        self.grid_layout.addWidget(button, row + 1, col + 2)

                elif row == 5:
                    if col <= 1:
                        self.grid_layout.addWidget(button, row + 1, col)
                        self.grid_layout.addWidget(QHLine(), row + 2, col)
                    elif col == 2:
                        self.grid_layout.addWidget(button, row + 1, col)
                        self.grid_layout.addWidget(QVLine(), row + 1, col + 1)
                        self.grid_layout.addWidget(QHLine(), row + 2, col)
                    elif 3 <= col <= 4:
                        self.grid_layout.addWidget(button, row + 1, col + 1)
                        self.grid_layout.addWidget(QHLine(), row + 2, col + 1)
                    elif col == 5:
                        self.grid_layout.addWidget(button, row + 1, col + 1)
                        self.grid_layout.addWidget(QVLine(), row + 1, col + 2)
                        self.grid_layout.addWidget(QHLine(), row + 2, col + 1)
                    elif 6 <= col <= 8:
                        self.grid_layout.addWidget(button, row + 1, col + 2)
                        self.grid_layout.addWidget(QHLine(), row + 2, col + 2)

                elif 6 <= row <= 8:
                    if col <= 1:
                        self.grid_layout.addWidget(button, row + 2, col)
                    elif col == 2:
                        self.grid_layout.addWidget(button, row + 2, col)
                        self.grid_layout.addWidget(QVLine(), row + 2, col + 1)
                    elif 3 <= col <= 4:
                        self.grid_layout.addWidget(button, row + 2, col + 1)
                    elif col == 5:
                        self.grid_layout.addWidget(button, row + 2, col + 1)
                        self.grid_layout.addWidget(QVLine(), row + 2, col + 2)
                    elif 6 <= col <= 8:
                        self.grid_layout.addWidget(button, row + 2, col + 2)
        except UnboundLocalError:
            self.generate_fields()
        except MaxRetriesExceeded:
            self.generate_fields()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Main()
    sys.exit(app.exec_())
